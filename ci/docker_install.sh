#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -yqq

#apt-get install php7.1-gd -yqq 

# Install git (the php image doesn't have it) which is required by composer
apt-get install git -yqq

#install composer
apt-get install wget -yqq
wget https://getcomposer.org/download/1.8.0/composer.phar
mv composer.phar /usr/local/bin/composer
chmod +x /usr/local/bin/composer
composer --version

# Install phpunit, the tool that we will use for testing
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit

# Install mysql driver
# Here you can install any other extension that you need
#docker-php-ext-install pdo_mysql

#export DEBIAN_FRONTEND=noninteractive
#apt-get -q -y install mysql-server
#mysql -uroot -p -e "CREATE DATABASE test123"
